const prodplugins = []//项目发布阶段需要用到的babel插件

// 生产环境移除console
if (process.env.NODE_ENV === 'production') {
  prodplugins.push('transform-remove-console')
}  

module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: prodplugins
}
