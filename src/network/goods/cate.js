import { request } from "../request";

export function getCategories(type, pagenum, pagesize) {
    return request({
        url: 'categories',
        method: 'get',
        params: {
            type,
            pagenum,
            pagesize
        }
    })
}

/**添加分类 */
export function addCategory(data) {
    return request({
        url: 'categories',
        method: 'post',
        data
    })
}

export function editCategory(id, data) {
    return request({
        url: `categories/${id}`,
        method: 'put',
        data
    })
}

export function deleteCateById(id) {
    return request({
        url: `categories/${id}`,
        method: 'delete'
    })
}