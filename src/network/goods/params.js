import { request } from "../request";

export function getCateParams(id, sel) {
    return request({
        url: `categories/${id}/attributes`,
        method: 'get',
        params: {
            sel
        }
    })
}

export function addParams(id, data) {
    return request({
        url: `categories/${id}/attributes`,
        method: 'post',
        data
    })
}

export function getParamById(id, attrId, sel) {
    return request({
        url: `categories/${id}/attributes/${attrId}`,
        method: 'get',
        params: {
            attr_sel: sel
        }
    })
}
export function editParamter(id, attrId, data) {
    return request({
        url: `categories/${id}/attributes/${attrId}`,
        method: 'put',
        data
    })
}

export function deleteParamter(id, attrId) {
    return request({
        url: `categories/${id}/attributes/${attrId}`,
        method: 'delete',
    })
}