import { method } from 'lodash'
import { request } from '../request'

export function getGoods(queryInfo) {
    return new request({
        url: 'goods',
        method: 'get',
        params: {
            query: queryInfo.query,
            pagenum: queryInfo.pagenum,
            pagesize: queryInfo.pagesize
        }
    })
}

export function deleteGoodsById(id) {
    return new request({
        url: `goods/${id}`,
        method: 'delete',

    })
}

export function addGoods(data) {
    return request({
        url: `goods`,
        method: "post",
        data
    })
}