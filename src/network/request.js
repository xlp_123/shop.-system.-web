import axios from 'axios'
import NProgress from 'nprogress'
// import 'nprogress/nprogress.css'

//node .\app.js
export function request(config) {
    console.log(config)

    const instance = axios.create({
        baseURL: 'http://127.0.0.1:8888/api/private/v1/',
        timeout: 5000
    })


    //请求拦截器全局添加token
    instance.interceptors.request.use(config => {
        NProgress.start();
        config.headers.Authorization = window.sessionStorage.getItem('token')
        return config;
    })

    //响应拦截器
    instance.interceptors.response.use(res => {
        NProgress.done();
        console.log(res)
        return res.data
    }, err => {
        console.log(err)
    })

    //直接返回一个instance对象，因为instance本身的返回值就是一个promise。
    return instance(config)

}