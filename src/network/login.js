import {request} from './request'

export function userLogin(username, password) {
    return request({
        url: 'login',
        method: 'post',
        data: {
            username,
            password
        }
    })
}