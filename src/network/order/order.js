import { method } from "lodash";
import { request } from "../request";

export function getOrders(queryInfo) {
    return request({
        url: 'orders', 
        method: 'get',
        params: {
            query: queryInfo.query,
            pagenum: queryInfo.pagenum,
            pagesize: queryInfo.pagesize
        }
    })
}

export function getOrderProgress(id) {
    return request({
        url: `kuaidi/${id}`,
        method: 'get'
    })
}