import { request } from "../request";

export function getRoles() {
    return request({
        url: 'roles',
        method: 'get'
    })
}

export function deleteRightById(roleId, rightId) {
    return request({
        url: `roles/${roleId}/rights/${rightId}`,
        method: 'delete'
    })
}

export function addRights(roleId,rightIds){
    return request({
        url: `roles/${roleId}/rights`,
        method:'post',
        data:{
            rids:rightIds
        }
    })
}