import { request } from '../request'

export function getUsers(queryInfo) {
  return request({
    url: 'users',
    method: 'get',
    params: {
      query: queryInfo.query,
      pagenum: queryInfo.pagenum,
      pagesize: queryInfo.pagesize
    }
  })
}

/**修改用户状态 uId:用户id  type:用户状态 true/false */
export function updateUserStatus(uId, type) {
  return request({
    url: `users/${uId}/state/${type}`,
    method: 'put'
  })
}

export function addUesr(user) {
  return request({
    url: 'users',
    method: 'post',
    data: user
    // params: {
    //   username: user.username,
    //   password: user.password,
    //   email: user.email,
    //   mobile: user.mobile,
    // }
  })
}

/* 根据id获取用户信息*/
export function getUserById(id) {
  return request({
    url: `users/${id}`,
    method: 'get',
  })
}

/**编辑用户 */
export function editUserById(id, data) {
  console.log(data)
  return request({
    url: `users/${id}`,
    method: 'put',
    data
  })
}
/**删除 */
export function deleteUserById(id) {
  return request({
    url: `users/${id}`,
    method: 'delete',
  })
}

export function setRole(id, rid) {
  return request({
    url: `users/${id}/role`,
    method: 'put',
    data: {
      rid
    }
  })
}