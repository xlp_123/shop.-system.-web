import Vue from 'vue'
import VueRouter from 'vue-router'
const Login = () => import('../views/Login.vue')
const Home = () => import('../views/Home.vue')
const Welcome = () => import('../views/Welcome.vue')
const Users = () => import('../views/user/Users.vue')
const Rights = () => import('../views/power/Rights.vue')
const Roles = () => import('../views/power/Roles.vue')
const Cate = () => import('../views/goods/Cate.vue')
const Params = () => import('../views/goods/Params.vue')
const List = () => import('../views/goods/List.vue')
const AddGoods = () => import('../views/goods/AddGoods.vue')
const Order = () => import('../views/order/Order.vue')
const Report = () => import("../views/report/Report.vue")

//1.安装插件
Vue.use(VueRouter)

const routes = [
  {
    path: '',
    redirect: 'login'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      {
        path: '/welcome',
        component: Welcome
      },
      {
        path: '/users',
        component: Users
      },
      {
        path: '/rights',
        component: Rights
      },
      {
        path: '/roles',
        component: Roles
      },
      {
        path: '/categories',
        component: Cate
      },
      {
        path: '/params',
        component: Params
      },
      {
        path: '/goods',
        component: List
      },
      {
        path: '/goods/add',
        component: AddGoods
      },
      {
        path: '/orders',
        component: Order
      },
      {
        path: '/reports',
        component: Report
      }
    ]
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

//全局导航守卫 前置钩子 路由跳转之前回调
router.beforeEach((to, from, next) => {
  //to：将要访问的路径。
  //from：从哪个路径跳转过来
  //next：函数，表示放行  next('/login') 强制跳转

  // document.title = to.matched[0].meta.title
  if (to.path === '/login') return next();

  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router
