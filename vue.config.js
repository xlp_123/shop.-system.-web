module.exports = {
    //自定义打包入口
    chainWebpack: config => {

        config.when(process.env.NODE_ENV === 'production', config => {
            config.entry('app').clear().add('@/main-prod.js')   

            //加载外部cdn资源
            config.set('externals', {
                vue: 'Vue',
                'vue-router': 'VueRouter',
                axios: 'axios',
                lodash: '_',
                echarts: 'echarts',
                nprogress: 'NProgress',
                'vue-quill-editor': 'VueQuillEditor'
            })

            //
            config.plugin('html').tap(args => {
                args[0].isProd = true
                return args
            })
        })


        config.when(process.env.NODE_ENV === 'development', config => {
            config.entry('app').clear().add('@/main-dev.js')

            config.plugin('html').tap(args => {
                args[0].isProd = false 
                return args
            })
        })
    }
}